/*!
 * \file Modulator.c
 * \author Шиганцов А.А.
 * \brief Модулятор NAVDAT
 */

#include "Modulator.h"
#include "qamConstellations.h"
#include "ofdmMaps.h"

#include "sdr_complex.h"
#include "sdr_fft.h"

#include <stdlib.h>

void NAVDAT_Modulator_setModeTIS(NAVDAT_Modulator_t* This, NAVDAT_TIS_qamMode_t mode)
{
  QAM_ModulatorCfg_t cfgTIS_Modulator;
  switch (mode)
  {
  case NAVDAT_TIS_qam4:
    cfgTIS_Modulator.Constellation = &NAVDAT_DRM_QAM4_Constellation;
    break;

  case NAVDAT_TIS_qam16:
    cfgTIS_Modulator.Constellation = &NAVDAT_DRM_QAM16_Constellation;
    break;

  default:
    cfgTIS_Modulator.Constellation = 0;
    break;
  }
  init_QAM_Modulator(&This->tisModulator, &cfgTIS_Modulator);
}

void NAVDAT_Modulator_setModeDS(NAVDAT_Modulator_t* This, NAVDAT_DS_qamMode_t mode)
{
  QAM_ModulatorCfg_t cfgDS_Modulator;
  switch (mode)
  {
  case NAVDAT_DS_qam4:
    cfgDS_Modulator.Constellation = &NAVDAT_DRM_QAM4_Constellation;
    break;

  case NAVDAT_DS_qam16:
    cfgDS_Modulator.Constellation = &NAVDAT_DRM_QAM16_Constellation;
    break;

  case NAVDAT_DS_qam64:
    cfgDS_Modulator.Constellation = &NAVDAT_DRM_QAM64_Constellation;
    break;

  default:
    cfgDS_Modulator.Constellation = 0;
    break;
  }
  init_QAM_Modulator(&This->dsModulator, &cfgDS_Modulator);
}

void init_NAVDAT_Modulator(NAVDAT_Modulator_t* This, NAVDAT_ModulatorCfg_t* Cfg)
{
  This->Parent = 0;
  This->on_data_sent = 0;
  This->fill_empty_buf = 0;

  QAM_ModulatorCfg_t cfgMIS_Modulator;
  cfgMIS_Modulator.Constellation = &NAVDAT_DRM_QAM4_Constellation;
  init_QAM_Modulator(&This->misModulator, &cfgMIS_Modulator);

  This->fftSizeFactor = Cfg->fftSizeFactor;
  This->fftAFactor = Cfg->fftAmplitudeFactor;
  This->fftSize = NAVDAT_Nfft(Cfg->fftSizeFactor);
  This->giSize = NAVDAT_OFDM_GUARD_INTERVAL_SAMPLES_COUNT(This->fftSizeFactor);

  NAVDAT_Modulator_setModeTIS(This, Cfg->qamTIS);
  NAVDAT_Modulator_setModeDS(This, Cfg->qamDS);

  NAVDAT_Modulator_reset(This);
}

/* возвращает колличество записанных символов */
static inline void data_modulator(NAVDAT_Modulator_t* This, QAM_Modulator_t* Modulator, qamBuffer_t* Data, Size_t* DataCounter, Size_t xIS_idxCount[], Int8_t* xIS_idx[], iqSample_t* Samples)
{
  Size_t CellsCounter = 0;
  while (CellsCounter < xIS_idxCount[This->FrameCounter])
  {
    Size_t idx = fft_idx(xIS_idx[This->FrameCounter][CellsCounter++], This->fftSize);

    qamSymbol_t sym;
    if (*DataCounter < Data->Size)
      sym = Data->P[(*DataCounter)++];
    else
      sym = rand()%Modulator->Constellation->SymbolsCount;

    qam_modulator_1(Modulator, sym, &Samples[idx]);
    Samples[idx] = iq_mul_real(Samples[idx], NAVDAT_aFactor(Modulator->Constellation->SymbolsCount) * This->fftAFactor);
  }
}

Overflow_t navdat_modulator_iq(NAVDAT_Modulator_t* This, iqSample_t* Samples, Size_t Count)
{ 
  SDR_ASSERT(Count == NAVDAT_OFDM_SYMBOL_SAMPLES_COUNT(This->fftSizeFactor));

  Overflow_t res = 0;

  if (!This->isData)
    if (This->fill_empty_buf) This->fill_empty_buf(This->Parent, Samples, Count);

  if (!This->isData)
  {
    if (This->ifft_samples_sink)
      This->ifft_samples_sink(This->Parent, Samples, This->fftSize);
    return res;
  }

  iqSample_t *fftSamples = &Samples[This->giSize];
  clear_buffer_iq(fftSamples, This->fftSize);

  /* Пилот-сигналы */
  Size_t i;
  for (i = 0; i < NAVDAT_Pilots_idxCount[This->FrameCounter]; i++)
  {
    Size_t idx = fft_idx(NAVDAT_Pilots_idx[This->FrameCounter][i], This->fftSize);
#ifndef NAVDAT_DEBUG_MODULATION_PILOTS_PHASE_CONST
    fftSamples[idx] = NAVDAT_Pilots_ph[This->FrameCounter][i];
#else
    fftSamples[idx] = (iqSample_t)NAVDAT_PILOT_PH;
#endif
    fftSamples[idx] = iq_mul_real(fftSamples[idx], This->fftAFactor);
  }

  /* Данные */
  // MIS
  data_modulator(This, &This->misModulator, &This->Data->mis, &This->DataCounter.mis, NAVDAT_MIS_idxCount, NAVDAT_MIS_idx, fftSamples);

  // TIS
  data_modulator(This, &This->tisModulator, &This->Data->tis, &This->DataCounter.tis, NAVDAT_TIS_idxCount, NAVDAT_TIS_idx, fftSamples);

  // DS
  data_modulator(This, &This->dsModulator, &This->Data->ds, &This->DataCounter.ds, NAVDAT_DS_idxCount, NAVDAT_DS_idx, fftSamples);

  if (This->ifft_samples_sink)
    This->ifft_samples_sink(This->Parent, fftSamples, This->fftSize);
  res |= ifft_iq(fftSamples, This->fftSize, fftSamples);

  /* Защитный интервал */
  copy_buffer_iq(&fftSamples[This->fftSize - This->giSize], This->giSize, Samples);

  This->FrameCounter++;
  if (This->FrameCounter >= NAVDAT_FRAME_ofdmSYMBOLS_COUNT)
  {
    This->FrameCounter = 0;
    This->isData = 0;

    SDR_ASSERT(This->DataCounter.mis == This->Data->mis.Size);
    SDR_ASSERT(This->DataCounter.tis == This->Data->tis.Size);
    SDR_ASSERT(This->DataCounter.ds == This->Data->ds.Size);
    NAVDAT_DataCounter_reset(&This->DataCounter);

    if (This->on_data_sent) This->on_data_sent(This->Parent);
  }

  return res;
}
