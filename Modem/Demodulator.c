/*!
 * \file Demodulator.c
 * \author Шиганцов А.А.
 * \brief Демодулятор сигнала NAVDAT
 */

#include "Demodulator.h"
#include "qamConstellations.h"
#include "ofdmMaps.h"
#include "ofdmSyncImage.h"

#include "sdr_complex.h"
#include "sdr_fft.h"
#include "sdr_math.h"
#include "sdr_sine.h"

#define QAM_DEMODULATOR_THRESHOLD 1

static void init_MIS_demodulator(NAVDAT_Demodulator_t *This)
{
  QAM_DemodulatorCfg_t cfgDem;
  cfgDem.Constellation = &NAVDAT_DRM_QAM4_Constellation;
  cfgDem.Threshold = QAM_DEMODULATOR_THRESHOLD;
  init_QAM_Demodulator(&This->misDemodulator, &cfgDem);
}

void NAVDAT_Demodulator_set_tisMode(NAVDAT_Demodulator_t *This, NAVDAT_TIS_qamMode_t mode)
{
  QAM_DemodulatorCfg_t cfgDem;
  switch(mode)
  {
  case NAVDAT_TIS_qam4:
    cfgDem.Constellation = &NAVDAT_DRM_QAM4_Constellation;
    break;

  case NAVDAT_TIS_qam16:
    cfgDem.Constellation = &NAVDAT_DRM_QAM16_Constellation;
    break;

  default:
    cfgDem.Constellation = 0;
    break;
  }
  cfgDem.Threshold = QAM_DEMODULATOR_THRESHOLD;
  init_QAM_Demodulator(&This->tisDemodulator, &cfgDem);
  This->isTIS = 1;
}

void NAVDAT_Demodulator_set_dsMode(NAVDAT_Demodulator_t *This, NAVDAT_DS_qamMode_t mode)
{
  QAM_DemodulatorCfg_t cfgDem;
  switch(mode)
  {
  case NAVDAT_DS_qam4:
    cfgDem.Constellation = &NAVDAT_DRM_QAM4_Constellation;
    break;

  case NAVDAT_DS_qam16:
    cfgDem.Constellation = &NAVDAT_DRM_QAM16_Constellation;
    break;

  case NAVDAT_DS_qam64:
    cfgDem.Constellation = &NAVDAT_DRM_QAM64_Constellation;
    break;

  default:
    cfgDem.Constellation = 0;
    break;
  }
  cfgDem.Threshold = QAM_DEMODULATOR_THRESHOLD;
  init_QAM_Demodulator(&This->dsDemodulator, &cfgDem);
  This->isDS = 1;
}

void init_NAVDAT_Demodulator(NAVDAT_Demodulator_t *This, NAVDAT_DemodulatorCfg_t *Cfg)
{
  This->Parent = 0;
  This->fft_samples_sink = 0;
  This->on_signal_detected = This->on_signal_not_detected = 0;
  This->on_dPh_ready = This->on_dA_ready = 0;
  This->on_mis_ready = This->on_tis_ready = This->on_ds_ready = 0;
  This->on_qam_symbol = 0;
  This->on_collector_ready = This->on_collector_processed = 0;

  This->IsExternalCollector = 0;

  This->fftSizeFactor = Cfg->fftSizeFactor;
  This->fftSize = NAVDAT_Nfft(Cfg->fftSizeFactor);
  This->DetectorTreshold = Cfg->DetectorTreshold;

  This->Data = Cfg->Data;
  This->DetectorBuf = Cfg->DetectorBuf;
  This->Collector = Cfg->Collector;
  This->dA = Cfg->aBuf;
  This->dPh = Cfg->phBuf;

  init_MIS_demodulator(This);
  This->isTIS = 0;
  This->isDS = 0;

  NAVDAT_Demodulator_reset(This);
}

void navdat_demodulator_signal_detector(NAVDAT_Demodulator_t* This, iqSample_t* fftSamples)
{
  Sample_t X = 0;
  iq_magnitude(fftSamples, This->fftSize, This->DetectorBuf.P);
  X = correlation(This->DetectorBuf.P, NAVDAT_OFDM_SYNCIMAGE_SIZE, NAVDAT_ofdmSyncImage);

  if ( ((!This->isData)&&(X > This->DetectorTreshold)) || ((This->isData)&&(This->Xcurrent < X)) )
  {
    This->Xcurrent = X;
    This->isData = 1;
    This->isTIS = 0;
    This->isDS = 0;
    This->FrameCounter = 0;
    NAVDAT_DataCounter_reset(&This->DataCounter);

    if (This->on_signal_detected)
      This->on_signal_detected(This->Parent);
  }
}

static inline Size_t rcf_idx(Int8_t cell_idx)
{
  Int16_t res = cell_idx;
#if NAVDAT_MODE == 'A'
  if (cell_idx < 0)
    res += 1;
  else
    res -= 2;
#endif
  res += NAVDAT_OFDM_SYMBOL_CELLS_COUNT/2;
  return res;
}

static inline Int8_t rcf_cell_idx(Size_t rcf_idx)
{
  Int16_t res = rcf_idx  - NAVDAT_OFDM_SYMBOL_CELLS_COUNT/2;
#if NAVDAT_MODE == 'A'
  if (res < 0)
    res -= 1;
  else
    res += 2;
#endif
  return res;
}

static inline void rcf_a(iqSample_t* x, iqSample_t* x0, Sample_t* r)
{
  *r = iq_magnitude_1(x0)/iq_magnitude_1(x);
}

static inline void rcf_ph_correction(Sample_t* phi, Sample_t* phi0)
{
  if (ABS(*phi - *phi0) >= SDR_PI)
  {
    if (*phi0 <= *phi)
    {
      if (*phi0 < 0)
        *phi0 += SDR_2PI;
      else
        *phi += SDR_2PI;
    }
    else
    {
      if (*phi0 > 0)
        *phi0 -= SDR_2PI;
      else
        *phi -= SDR_2PI;
    }
  }
}

static inline void rcf_ph(iqSample_t* x, iqSample_t* x0, Sample_t* r)
{
  Sample_t
      phi0 = iq_angle_1(x0),
      phi = iq_angle_1(x);
  rcf_ph_correction(&phi, &phi0);
  *r = phi0 - phi;
}

static inline void rcf_interp(Size_t i0, Sample_t x0, Size_t i, Sample_t x, Size_t i_r, Sample_t* r, Size_t n)
{
  Sample_t
      A = x0 - x,
      B = (Sample_t)i0 - i,
      C = x*i0 - x0*i;

  Size_t j;
  for (j = 0; j < n; j++)
    r[j] = (A*(i_r+j) + C)/B;
}

/* возвращает количкство точек между пилотными несущеми */
Size_t rcf_proc(Bool_t isLast, Size_t idx_previous, Size_t idx, Buffer_t* buf)
{
  Size_t i;
  Value_t n;
  if (buf->P[0] == 0)
  {
    i = 0;
    n = idx;
  }
  else if (isLast && (buf->P[buf->Size-1] == 0))
  {
    i = idx_previous+1;
    n = buf->Size-idx_previous;
    if (idx == buf->Size-1)
    {
      idx = idx_previous;
      idx_previous = idx-1;
    }
  }
  else
  {
    i = idx_previous+1;
    n = idx-idx_previous-1;
  }
  if (n <= 0) return 0;
  rcf_interp(idx_previous, buf->P[idx_previous], idx, buf->P[idx], i, &buf->P[i], n);
  return n;
}

#ifdef NAVDAT_DEBUG_MODULATION_PILOTS_PHASE_CONST
static iqSample_t pilotSym = NAVDAT_PILOT_PH;
#endif
Overflow_t navdat_demodulator_signal_recovery(NAVDAT_Demodulator_t* This, iqSample_t* fftSamples)
{
  Overflow_t res = 0;

  clear_buffer(This->dPh.P, This->dPh.Size);
  if (This->FrameCounter % NAVDAT_GAIN_PILOTS_PERIOD == 0)
    clear_buffer(This->dA.P, This->dA.Size);

  Size_t CellsCount = NAVDAT_Pilots_idxCount[This->FrameCounter], CellsCounter = 0,
      idx_previous = 0;
  while (CellsCounter < CellsCount)
  {
    Size_t idx = fft_idx(NAVDAT_Pilots_idx[This->FrameCounter][CellsCounter], This->fftSize),
        idxR = rcf_idx(NAVDAT_Pilots_idx[This->FrameCounter][CellsCounter]);

#ifndef NAVDAT_DEBUG_MODULATION_PILOTS_PHASE_CONST
    rcf_ph(&fftSamples[idx], &NAVDAT_Pilots_ph[This->FrameCounter][CellsCounter], &This->dPh.P[idxR]);
    rcf_a(&fftSamples[idx], &NAVDAT_Pilots_ph[This->FrameCounter][CellsCounter], &This->dA.P[idxR]);
#else
    recovery_factor_ph(&pilotSym, &fftSamples[idx], &This->dPh.P[idxR]);
    recovery_factor_a(&pilotSym, &fftSamples[idx], &This->dA.P[idxR]);
#endif

    if (CellsCounter > 0)
    {
      rcf_ph_correction(&This->dPh.P[idxR], &This->dPh.P[idx_previous]);
      rcf_proc((CellsCounter == CellsCount-1), idx_previous, idxR, &This->dPh);
    }

    idx_previous = idxR;
    CellsCounter++;
  }
  if (This->on_dPh_ready)
    This->on_dPh_ready(This->Parent);

  Size_t i;
  for (i = 0; i < This->dPh.Size; i++)
  {
    Int8_t cell_idx = rcf_cell_idx(i);
    Size_t idx = fft_idx(cell_idx, This->fftSize);

    iqSample_t factor;
    sinus_iq_1(This->dPh.P[i], &factor);
    fftSamples[idx] = iq_mul(fftSamples[idx], factor);
  }

  if (This->FrameCounter % NAVDAT_GAIN_PILOTS_PERIOD == NAVDAT_GAIN_PILOTS_PERIOD - 1)
  {
    Size_t j = 0;
    idx_previous = 0;
    for (i = 0; i < This->dA.Size; i++)
    {
      Bool_t isLast = (i == This->dA.Size-1);
      if ((This->dA.P[i] != 0) || isLast)
      {
        rcf_proc(isLast, idx_previous, i, &This->dA);

        idx_previous = i;
      }
    }
    if (This->on_dA_ready)
      This->on_dA_ready(This->Parent);

    Size_t
        coll_idx0 = (This->FrameCounter - (NAVDAT_GAIN_PILOTS_PERIOD - 1))*This->fftSize,
        coll_idxM = (This->FrameCounter + 1)*This->fftSize;
    for (j = coll_idx0; j < coll_idxM; j += This->fftSize)
      for (i = 0; i < This->dA.Size; i++)
      {
        Int8_t cell_idx = rcf_cell_idx(i);
        Size_t idx = j + fft_idx(cell_idx, This->fftSize);
        This->Collector.P[idx] = iq_mul_real(This->Collector.P[idx], This->dA.P[i]);
      }
  }

  return res;
}

Sample_t navdat_demodulator_snr_estimation(NAVDAT_Demodulator_t *This, Size_t start_idx, Size_t stop_idx)
{
  Sample_t* daP = &This->dA.P[start_idx];
  Size_t Count = stop_idx;
  SDR_ASSERT (Count <= This->dA.Size);

  Sample_t Mx = mean(daP, Count);
  Sample_t D = variance_m(Mx, daP, Count);
  Sample_t g = sqrt_1(D);
  Sample_t snr = to_dB(Mx/g);
  return snr;
}

Overflow_t navdat_demodulator_mis(NAVDAT_Demodulator_t *This, Size_t FrameIdx, iqSample_t *fftSamples)
{
  if (This->DataCounter.mis == NAVDAT_MIS_CELLS_COUNT)
    return 0;

  Overflow_t res = 0;

  Size_t CellsCounter = 0;
  while (CellsCounter < NAVDAT_MIS_idxCount[FrameIdx])
  {
    Size_t idx = fft_idx(NAVDAT_MIS_idx[FrameIdx][CellsCounter++], This->fftSize);

    fftSamples[idx] = iq_div_real(fftSamples[idx], NAVDAT_A0);
    qam_demodulator_1(&This->misDemodulator, &fftSamples[idx], &This->Data->mis.P[This->DataCounter.mis++]);

    if (This->on_qam_symbol)
      This->on_qam_symbol(This->Parent, fftSamples[idx]);
  }
  if (This->DataCounter.mis == NAVDAT_MIS_CELLS_COUNT)
  {
    This->Data->mis.Size = NAVDAT_MIS_CELLS_COUNT;
    if (This->on_mis_ready)
      This->on_mis_ready(This->Parent, &This->Data->mis);
  }

  return res;
}

Overflow_t navdat_demodulator_tis(NAVDAT_Demodulator_t *This, Size_t FrameIdx, iqSample_t *fftSamples)
{
  if (This->DataCounter.tis == NAVDAT_TIS_CELLS_COUNT)
    return 0;
  if (!This->tisDemodulator.Constellation)
    return 0;

  Overflow_t res = 0;

  Size_t CellsCounter = 0;
  while (CellsCounter < NAVDAT_TIS_idxCount[FrameIdx])
  {
    Size_t idx = fft_idx(NAVDAT_TIS_idx[FrameIdx][CellsCounter++], This->fftSize);

    fftSamples[idx] = iq_div_real(fftSamples[idx], NAVDAT_aFactor(This->tisDemodulator.Constellation->SymbolsCount));
    qam_demodulator_1(&This->tisDemodulator, &fftSamples[idx], &This->Data->tis.P[This->DataCounter.tis++]);

    if (This->on_qam_symbol)
      This->on_qam_symbol(This->Parent, fftSamples[idx]);
  }
  if (This->DataCounter.tis == NAVDAT_TIS_CELLS_COUNT)
  {
    This->Data->tis.Size = NAVDAT_TIS_CELLS_COUNT;
    if (This->on_tis_ready)
      This->on_tis_ready(This->Parent, &This->Data->tis);
  }

  return res;
}

Overflow_t navdat_demodulator_ds(NAVDAT_Demodulator_t *This, Size_t FrameIdx, iqSample_t *fftSamples)
{
  if (This->DataCounter.ds == NAVDAT_DS_CELLS_COUNT)
    return 0;
  if (!This->dsDemodulator.Constellation)
    return 0;

  Overflow_t res = 0;

  Size_t CellsCounter = 0;
  while (CellsCounter < NAVDAT_DS_idxCount[FrameIdx])
  {
    Size_t idx = fft_idx(NAVDAT_DS_idx[FrameIdx][CellsCounter++], This->fftSize);

    fftSamples[idx] = iq_div_real(fftSamples[idx], NAVDAT_aFactor(This->dsDemodulator.Constellation->SymbolsCount));
    qam_demodulator_1(&This->dsDemodulator, &fftSamples[idx], &This->Data->ds.P[This->DataCounter.ds++]);

    if (This->on_qam_symbol)
      This->on_qam_symbol(This->Parent, fftSamples[idx]);
  }
  if (This->DataCounter.ds == NAVDAT_DS_CELLS_COUNT)
  {
    This->Data->ds.Size = NAVDAT_DS_CELLS_COUNT;
    if (This->on_ds_ready)
      This->on_ds_ready(This->Parent, &This->Data->ds);
  }

  return res;
}

Overflow_t navdat_demodulator_collector_handler(NAVDAT_Demodulator_t* This, iqSample_t* Collector)
{
  Overflow_t res = 0;

  Size_t i;
  for (i = 0; i < NAVDAT_FRAME_ofdmSYMBOLS_COUNT; i++)
  {
    Size_t idx = i*This->fftSize;
    res |= navdat_demodulator_mis(This, i, &Collector[idx]);
  }
  for (i = 0; i < NAVDAT_FRAME_ofdmSYMBOLS_COUNT; i++)
  {
    Size_t idx = i*This->fftSize;
    if (This->isTIS)
      res |= navdat_demodulator_tis(This, i, &Collector[idx]);
    if (This->isDS)
      res |= navdat_demodulator_ds(This, i, &Collector[idx]);
  }

  return res;
}

Overflow_t navdat_demodulator_iq(NAVDAT_Demodulator_t* This, iqSample_t* Samples, Size_t Count)
{
  SDR_ASSERT(Count == This->fftSize);

  Overflow_t res = 0;

  Size_t idx = This->FrameCounter*This->fftSize;
  SDR_ASSERT(idx + This->fftSize <= This->Collector.Size);

  iqSample_t* fftSamples = &This->Collector.P[idx];
  res |= fft_iq(Samples, This->fftSize, fftSamples);
  if (This->fft_samples_sink)
    This->fft_samples_sink(This->Parent, fftSamples, This->fftSize);

  navdat_demodulator_signal_detector(This, fftSamples);

  if (!This->isData)
  {
    if (This->on_signal_not_detected)
      This->on_signal_not_detected(This->Parent);

    return res;
  }

  res |= navdat_demodulator_signal_recovery(This, fftSamples);

  This->FrameCounter++;
  if (This->FrameCounter >= NAVDAT_FRAME_ofdmSYMBOLS_COUNT)
  {
    if (This->on_collector_ready)
      This->on_collector_ready(This->Parent);
    if (!This->IsExternalCollector)
      res |= navdat_demodulator_collector_handler(This, This->Collector.P);
    if (This->on_collector_processed)
      This->on_collector_processed(This->Parent);

    This->FrameCounter = 0;
    This->isData = 0;
    This->isTIS = 0;
    This->isDS = 0;
  }

  return res;
}
