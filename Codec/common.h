/*!
 * \file common.h
 * \author Шиганцов А.А.
 * \brief Общие елементы для кодирования NAVDAT
 */

#ifndef __NAVDAT_ENCODING_COMMON_H__
#define __NAVDAT_ENCODING_COMMON_H__

#include "SDR/BASE/common.h"

#ifdef __cplusplus
extern "C"{
#endif

typedef enum {NAVDAT_xIS_Code_None, NAVDAT_xIS_Code_Max} NAVDAT_xIS_Code_t;
typedef enum {NAVDAT_DS_Code_None, NAVDAT_DS_Code_Min, NAVDAT_DS_Code_Normal, NAVDAT_DS_Code_Max} NAVDAT_DS_Code_t;

Size_t NAVDAT_Code_xisCountMax(NAVDAT_xIS_Code_t code, Size_t maxRawDataCount);
Size_t NAVDAT_Code_dsCountMax(NAVDAT_DS_Code_t code, Size_t maxRawDataCount);

Size_t NAVDAT_Code_xisRawCountMax(NAVDAT_xIS_Code_t code, Size_t maxDataCount);
Size_t NAVDAT_Code_dsRawCountMax(NAVDAT_DS_Code_t code, Size_t maxDataCount);


#ifdef __cplusplus
}
#endif

#endif // __NAVDAT_ENCODING_COMMON_H__
