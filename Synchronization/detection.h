#ifndef SDR_NAVDAT_DETECTION_H
#define SDR_NAVDAT_DETECTION_H

#include "SDR/BASE/common.h"

#ifdef __cplusplus
extern "C"{
#endif

typedef struct
{
  Sample_t Treshold;
  Size_t Step;

  iqBuffer_t fftBuf;
  Buffer_t magBuf;
} NAVDAT_SignalDetectorCfg_t;

typedef struct
{
  Sample_t Treshold, Current;
  Size_t Step;

  iqBuffer_t fftBuf;
  Buffer_t magBuf;
} NAVDAT_SignalDetector_t;

void init_NAVDAT_SignalDetector(NAVDAT_SignalDetector_t* This, NAVDAT_SignalDetectorCfg_t* Cfg);

Sample_t navdat_detect_signal(Sample_t* fftMagnitude, Size_t Count);

Bool_t navdat_signal_detector(NAVDAT_SignalDetector_t* This, iqSample_t* Samples, Size_t Count);

#ifdef __cplusplus
}
#endif

#endif // SDR_NAVDAT_DETECTION_H
