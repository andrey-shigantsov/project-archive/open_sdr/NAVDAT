/*!
 * \file Modulator.h
 * \author Шиганцов А.А.
 * \brief Заголовочный файл модулятора NAVDAT
 */

#ifndef __MODULATOR_NAVDAT_H__
#define __MODULATOR_NAVDAT_H__

#include "common.h"
#include "SDR/BASE/Modulation/digital/qamModulator.h"

#ifdef __cplusplus
extern "C"{
#endif

typedef struct
{
  NAVDAT_TIS_qamMode_t qamTIS;
  NAVDAT_DS_qamMode_t qamDS;

  Int8_t fftSizeFactor;
  Sample_t fftAmplitudeFactor;
} NAVDAT_ModulatorCfg_t;

typedef struct
{
  event_sink_t on_data_sent;
  samples_sink_iq_t fill_empty_buf;
  samples_sink_iq_t ifft_samples_sink;
} NAVDAT_ModulatorCallback_t;

typedef struct
{
  void* Parent;

  QAM_Modulator_t misModulator, tisModulator, dsModulator;

  Int8_t fftSizeFactor;
  Size_t fftSize, giSize;
  Sample_t fftAFactor;

  Bool_t isData;
  NAVDAT_qamData_t* Data;

  Size_t FrameCounter;
  NAVDAT_DataCounter_t DataCounter;

  event_sink_t on_data_sent;
  samples_sink_iq_t fill_empty_buf;
  samples_sink_iq_t ifft_samples_sink;
} NAVDAT_Modulator_t;

INLINE void NAVDAT_Modulator_set_callback(NAVDAT_Modulator_t* This, void* Parent,
                                          NAVDAT_ModulatorCallback_t* Callback)
{
  This->Parent = Parent;
  This->on_data_sent = Callback->on_data_sent;
  This->fill_empty_buf = Callback->fill_empty_buf;
  This->ifft_samples_sink = Callback->ifft_samples_sink;
}

void NAVDAT_Modulator_setModeTIS(NAVDAT_Modulator_t* This, NAVDAT_TIS_qamMode_t mode);
void NAVDAT_Modulator_setModeDS(NAVDAT_Modulator_t* This, NAVDAT_DS_qamMode_t mode);

INLINE Bool_t NAVDAT_Modulator_isTransmit(NAVDAT_Modulator_t* This)
{
  return This->isData;
}

INLINE Bool_t NAVDAT_Modulator_transmit(NAVDAT_Modulator_t* This, NAVDAT_qamData_t* Data)
{
  if (This->isData) return 0;

  SDR_ASSERT(Data->mis.Size <= NAVDAT_MIS_CELLS_COUNT);
  SDR_ASSERT(Data->tis.Size <= NAVDAT_TIS_CELLS_COUNT);
  SDR_ASSERT(Data->ds.Size <= NAVDAT_DS_CELLS_COUNT);

  This->Data = Data;
  This->FrameCounter = 0;

  NAVDAT_Modulator_setModeTIS(This, This->Data->tisMode);
  NAVDAT_Modulator_setModeDS(This, This->Data->dsMode);

  This->isData = 1;

  return 1;
}

INLINE void NAVDAT_Modulator_reset(NAVDAT_Modulator_t* This)
{
  This->isData = 0;
  This->FrameCounter = 0;
  NAVDAT_DataCounter_reset(&This->DataCounter);
}

void init_NAVDAT_Modulator(NAVDAT_Modulator_t* This, NAVDAT_ModulatorCfg_t* Cfg);
Overflow_t navdat_modulator_iq(NAVDAT_Modulator_t* This, iqSample_t* Samples, Size_t Count);

#ifdef __cplusplus
}
#endif

#endif // __MODULATOR_NAVDAT_H__
