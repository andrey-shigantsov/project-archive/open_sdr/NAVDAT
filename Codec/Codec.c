/*!
 * \file Encoding.c
 * \author Шиганцов А.А.
 * \brief Кодирование NAVDAT
 */

#include "Codec.h"

#define XIS_NONE_CRC LIQUID_CRC_8
#define XIS_NONE_FEC_INNER LIQUID_FEC_NONE
#define XIS_NONE_FEC_OUTER LIQUID_FEC_NONE

#define XIS_MAX_CRC LIQUID_CRC_8
#define XIS_MAX_FEC_INNER LIQUID_FEC_NONE
#define XIS_MAX_FEC_OUTER LIQUID_FEC_CONV_V39

#define DS_NONE_CRC LIQUID_CRC_32
#define DS_NONE_FEC_INNER LIQUID_FEC_NONE
#define DS_NONE_FEC_OUTER LIQUID_FEC_NONE

#define DS_MIN_CRC LIQUID_CRC_32
#define DS_MIN_FEC_INNER LIQUID_FEC_RS_M8
#define DS_MIN_FEC_OUTER LIQUID_FEC_CONV_V29P78

#define DS_NORM_CRC LIQUID_CRC_32
#define DS_NORM_FEC_INNER LIQUID_FEC_RS_M8
#define DS_NORM_FEC_OUTER LIQUID_FEC_CONV_V29P23

#define DS_MAX_CRC LIQUID_CRC_32
#define DS_MAX_FEC_INNER LIQUID_FEC_RS_M8
#define DS_MAX_FEC_OUTER LIQUID_FEC_CONV_V29

static Size_t get_maxDataCount(Size_t maxRawDataCount, int crc, int fec1, int fec2)
{
  Size_t maxDataCount = maxRawDataCount;
  while (packetizer_compute_enc_msg_len(maxDataCount, crc, fec1, fec2) > maxRawDataCount)
    maxDataCount--;
  return maxDataCount;
}

Size_t NAVDAT_Code_xisRawCountMax(NAVDAT_xIS_Code_t code, Size_t maxDataCount)
{
  int crc, fec1, fec2;
  switch(code)
  {
  default:
    return -1;

  case NAVDAT_xIS_Code_None:
    crc = XIS_NONE_CRC; fec1 = XIS_NONE_FEC_INNER; fec2 = XIS_NONE_FEC_OUTER;
    break;

  case NAVDAT_xIS_Code_Max:
    crc = XIS_MAX_CRC; fec1 = XIS_MAX_FEC_INNER; fec2 = XIS_MAX_FEC_OUTER;
    break;
  }
  return packetizer_compute_enc_msg_len(maxDataCount, crc, fec1, fec2);
}

Size_t NAVDAT_Code_xisCountMax(NAVDAT_xIS_Code_t code, Size_t maxRawDataCount)
{
  int crc, fec1, fec2;
  switch(code)
  {
  default:
    return -1;

  case NAVDAT_xIS_Code_None:
    crc = XIS_NONE_CRC; fec1 = XIS_NONE_FEC_INNER; fec2 = XIS_NONE_FEC_OUTER;
    break;

  case NAVDAT_xIS_Code_Max:
    crc = XIS_MAX_CRC; fec1 = XIS_MAX_FEC_INNER; fec2 = XIS_MAX_FEC_OUTER;
    break;
  }
  return get_maxDataCount(maxRawDataCount, crc, fec1, fec2);
}

Size_t NAVDAT_Code_dsRawCountMax(NAVDAT_DS_Code_t code, Size_t maxDataCount)
{
  int crc, fec1, fec2;
  switch(code)
  {
  default:
    return -1;

  case NAVDAT_DS_Code_None:
    crc = DS_NONE_CRC; fec1 = DS_NONE_FEC_INNER; fec2 = DS_NONE_FEC_OUTER;
    break;

  case NAVDAT_DS_Code_Min:
    crc = DS_MIN_CRC; fec1 = DS_MIN_FEC_INNER; fec2 = DS_MIN_FEC_OUTER;
    break;

  case NAVDAT_DS_Code_Normal:
    crc = DS_NORM_CRC; fec1 = DS_NORM_FEC_INNER; fec2 = DS_NORM_FEC_OUTER;
    break;

  case NAVDAT_DS_Code_Max:
    crc = DS_MAX_CRC; fec1 = DS_MAX_FEC_INNER; fec2 = DS_MAX_FEC_OUTER;
    break;
  }
  return packetizer_compute_enc_msg_len(maxDataCount, crc, fec1, fec2);
}

Size_t NAVDAT_Code_dsCountMax(NAVDAT_DS_Code_t code, Size_t maxRawDataCount)
{
  int crc, fec1, fec2;
  switch(code)
  {
  default:
    return -1;

  case NAVDAT_DS_Code_None:
    crc = DS_NONE_CRC; fec1 = DS_NONE_FEC_INNER; fec2 = DS_NONE_FEC_OUTER;
    break;

  case NAVDAT_DS_Code_Min:
    crc = DS_MIN_CRC; fec1 = DS_MIN_FEC_INNER; fec2 = DS_MIN_FEC_OUTER;
    break;

  case NAVDAT_DS_Code_Normal:
    crc = DS_NORM_CRC; fec1 = DS_NORM_FEC_INNER; fec2 = DS_NORM_FEC_OUTER;
    break;

  case NAVDAT_DS_Code_Max:
    crc = DS_MAX_CRC; fec1 = DS_MAX_FEC_INNER; fec2 = DS_MAX_FEC_OUTER;
    break;
  }
  return get_maxDataCount(maxRawDataCount, crc, fec1, fec2);
}

void init_NAVDAT_Codec(NAVDAT_Codec_t *This, NAVDAT_CodecCfg_t *Cfg)
{
  This->tisMode = Cfg->tisMode;
  This->dsMode = Cfg->dsMode;
  This->xisCode = Cfg->xisCode;
  This->dsCode = Cfg->dsCode;

  Size_t tmpSize = 0;
  tmpSize = NAVDAT_Code_xisRawCountMax(Cfg->xisCode, NAVDAT_misCountMax(Cfg->xisCode));
  SDR_ASSERT(tmpSize <= NAVDAT_misRawCountMax());
  tmpSize = NAVDAT_Code_xisRawCountMax(Cfg->xisCode, NAVDAT_tisCountMax(Cfg->tisMode, Cfg->xisCode));
  SDR_ASSERT(tmpSize <= NAVDAT_tisRawCountMax(Cfg->tisMode));
  tmpSize = NAVDAT_Code_dsRawCountMax(Cfg->dsCode, NAVDAT_dsCountMax(Cfg->dsMode, Cfg->dsCode));
  SDR_ASSERT(tmpSize <= NAVDAT_dsRawCountMax(Cfg->dsMode));

  switch(This->xisCode)
  {
  case NAVDAT_xIS_Code_None:
    This->misPack = packetizer_create(NAVDAT_misCountMax(This->xisCode), XIS_NONE_CRC, XIS_NONE_FEC_INNER, XIS_NONE_FEC_OUTER);
    This->tisPack = packetizer_create(NAVDAT_tisCountMax(This->tisMode, This->xisCode), XIS_NONE_CRC, XIS_NONE_FEC_INNER, XIS_NONE_FEC_OUTER);
    break;

  case NAVDAT_xIS_Code_Max:
    This->misPack = packetizer_create(NAVDAT_misCountMax(This->xisCode), XIS_MAX_CRC, XIS_MAX_FEC_INNER, XIS_MAX_FEC_OUTER);
    This->tisPack = packetizer_create(NAVDAT_tisCountMax(This->tisMode, This->xisCode), XIS_MAX_CRC, XIS_MAX_FEC_INNER, XIS_MAX_FEC_OUTER);
    break;
  }

  switch(This->dsCode)
  {
  case NAVDAT_DS_Code_None:
    This->dsPack = packetizer_create(NAVDAT_dsCountMax(This->dsMode, This->dsCode), DS_NONE_CRC, DS_NONE_FEC_INNER, DS_NONE_FEC_OUTER);
    break;

  case NAVDAT_DS_Code_Min:
    This->dsPack = packetizer_create(NAVDAT_dsCountMax(This->dsMode, This->dsCode), DS_MIN_CRC, DS_MIN_FEC_INNER, DS_MIN_FEC_OUTER);
    break;

  case NAVDAT_DS_Code_Normal:
    This->dsPack = packetizer_create(NAVDAT_dsCountMax(This->dsMode, This->dsCode), DS_NORM_CRC, DS_NORM_FEC_INNER, DS_NORM_FEC_OUTER);
    break;

  case NAVDAT_DS_Code_Max:
    This->dsPack = packetizer_create(NAVDAT_dsCountMax(This->dsMode, This->dsCode), DS_MAX_CRC, DS_MAX_FEC_INNER, DS_MAX_FEC_OUTER);
    break;
  }

  This->NeedReinitTIS = This->NeedReinitDS = 0;
}

void navdat_encode(NAVDAT_Codec_t *This, NAVDAT_RawFrame_t *inputFrame, NAVDAT_RawFrame_t *outputFrame)
{
  SDR_ASSERT((!This->NeedReinitTIS)&&(!This->NeedReinitDS));

  outputFrame->tisMode = inputFrame->tisMode;
  outputFrame->dsMode = inputFrame->dsMode;

  packetizer_encode(This->misPack, inputFrame->misData, outputFrame->misData);
  packetizer_encode(This->tisPack, inputFrame->tisData, outputFrame->tisData);
  packetizer_encode(This->dsPack, inputFrame->dsData, outputFrame->dsData);

  outputFrame->Count.mis = packetizer_get_enc_msg_len(This->misPack);
  outputFrame->Count.tis = packetizer_get_enc_msg_len(This->tisPack);
  outputFrame->Count.ds = packetizer_get_enc_msg_len(This->dsPack);
}

Bool_t navdat_decode_mis(NAVDAT_Codec_t *This, NAVDAT_RawFrame_t *inputFrame, NAVDAT_RawFrame_t *outputFrame)
{
  outputFrame->Count.mis = packetizer_get_dec_msg_len(This->misPack);
  return packetizer_decode(This->misPack, inputFrame->misData, outputFrame->misData);
}

Bool_t navdat_decode_tis(NAVDAT_Codec_t *This, NAVDAT_RawFrame_t *inputFrame, NAVDAT_RawFrame_t *outputFrame)
{
  SDR_ASSERT(!This->NeedReinitTIS);
  outputFrame->tisMode = This->tisMode;
  outputFrame->Count.tis = packetizer_get_dec_msg_len(This->tisPack);
  return packetizer_decode(This->tisPack, inputFrame->tisData, outputFrame->tisData);
}

Bool_t navdat_decode_ds(NAVDAT_Codec_t *This, NAVDAT_RawFrame_t *inputFrame, NAVDAT_RawFrame_t *outputFrame)
{
  SDR_ASSERT(!This->NeedReinitDS);
  outputFrame->dsMode = inputFrame->dsMode;
  outputFrame->Count.ds = packetizer_get_dec_msg_len(This->dsPack);
  return packetizer_decode(This->dsPack, inputFrame->dsData, outputFrame->dsData);
}

void deinit_NAVDAT_Codec(NAVDAT_Codec_t* This)
{
  packetizer_destroy(This->misPack);
  packetizer_destroy(This->tisPack);
  packetizer_destroy(This->dsPack);
}

void NAVDAT_Codec_reinitTIS(NAVDAT_Codec_t* This)
{
  if (!This->NeedReinitTIS) return;

  This->NeedReinitTIS = 0;
  switch(This->xisCode)
  {
  case NAVDAT_xIS_Code_None:
    This->tisPack = packetizer_recreate(This->tisPack, NAVDAT_tisCountMax(This->tisMode, This->xisCode), XIS_NONE_CRC, XIS_NONE_FEC_INNER, XIS_NONE_FEC_OUTER);
    break;

  case NAVDAT_xIS_Code_Max:
    This->tisPack = packetizer_recreate(This->tisPack, NAVDAT_tisCountMax(This->tisMode, This->xisCode), XIS_MAX_CRC, XIS_MAX_FEC_INNER, XIS_MAX_FEC_OUTER);
    break;
  }
}

void NAVDAT_Codec_reinitDS(NAVDAT_Codec_t* This)
{
  if (!This->NeedReinitDS) return;

  This->NeedReinitDS = 0;
  switch(This->dsCode)
  {
  case NAVDAT_DS_Code_None:
    This->dsPack = packetizer_recreate(This->dsPack, NAVDAT_dsCountMax(This->dsMode, This->dsCode), DS_NONE_CRC, DS_NONE_FEC_INNER, DS_NONE_FEC_OUTER);
    break;

  case NAVDAT_DS_Code_Min:
    This->dsPack = packetizer_recreate(This->dsPack, NAVDAT_dsCountMax(This->dsMode, This->dsCode), DS_MIN_CRC, DS_MIN_FEC_INNER, DS_MIN_FEC_OUTER);
    break;

  case NAVDAT_DS_Code_Normal:
    This->dsPack = packetizer_recreate(This->dsPack, NAVDAT_dsCountMax(This->dsMode, This->dsCode), DS_NORM_CRC, DS_NORM_FEC_INNER, DS_NORM_FEC_OUTER);
    break;

  case NAVDAT_DS_Code_Max:
    This->dsPack = packetizer_recreate(This->dsPack, NAVDAT_dsCountMax(This->dsMode, This->dsCode), DS_MAX_CRC, DS_MAX_FEC_INNER, DS_MAX_FEC_OUTER);
    break;
  }
}
