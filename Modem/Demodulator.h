/*!
 * \file Demodulator.h
 * \author Шиганцов А.А.
 * \brief Заголовочный файл демодулятора сигнала NAVDAT
 */

#ifndef __NAVDAT_DEMODULATOR_H__
#define __NAVDAT_DEMODULATOR_H__

#include "common.h"
#include "SDR/BASE/Modulation/digital/qamDemodulator.h"

#ifdef __cplusplus
extern "C"{
#endif

#define NAVDAT_DEMODULATOR_COLLECTOR_SIZE(fftIdxFactor) ((Size_t)NAVDAT_FRAME_ofdmSYMBOLS_COUNT*NAVDAT_Nfft(fftIdxFactor))

typedef struct
{
  Int8_t fftSizeFactor;
  Sample_t DetectorTreshold;

  NAVDAT_qamData_t* Data;
  Buffer_t DetectorBuf; /* Размер буфера: NAVDAT_Nfft(fftIdxFactor) */
  iqBuffer_t Collector; /* Размер буфера: NAVDAT_DEMODULATOR_COLLECTOR_SIZE(fftIdxFactor) */
  Buffer_t aBuf, phBuf; /* Размер буфера: NAVDAT_OFDM_SYMBOL_CELLS_COUNT */
} NAVDAT_DemodulatorCfg_t;

typedef struct
{
  qambuffer_sink_t on_mis_ready, on_tis_ready, on_ds_ready;
  samples_sink_iq_t fft_samples_sink;
  event_sink_t on_signal_detected, on_signal_not_detected, on_dPh_ready, on_dA_ready, on_collector_ready, on_collector_processed;
  sample_sink_iq_t on_qam_symbol;
} NAVDAT_DemodulatorCallback_t;

typedef struct
{
  void* Parent;

  Int8_t fftSizeFactor;
  Size_t fftSize;
  Sample_t DetectorTreshold;

  QAM_Demodulator_t misDemodulator, tisDemodulator, dsDemodulator;
  NAVDAT_qamData_t* Data;
  Buffer_t DetectorBuf;
  iqBuffer_t Collector;
  Buffer_t dA, dPh;

#ifndef NAVDAT_DEMOD_NO_FREQ_SYNC
  int Xidx;
#endif
  Sample_t Xcurrent;

  Bool_t isData, isTIS, isDS;
  Size_t FrameCounter;
  NAVDAT_DataCounter_t DataCounter;

  Bool_t IsExternalCollector;

  qambuffer_sink_t on_mis_ready, on_tis_ready, on_ds_ready;
  samples_sink_iq_t fft_samples_sink;
  event_sink_t on_signal_detected, on_signal_not_detected, on_dPh_ready, on_dA_ready, on_collector_ready, on_collector_processed;
  sample_sink_iq_t on_qam_symbol;
} NAVDAT_Demodulator_t;

INLINE void NAVDAT_Demodulator_set_callback(NAVDAT_Demodulator_t* This, void* Parent, NAVDAT_DemodulatorCallback_t* Callback)
{
  This->Parent = Parent;
  This->fft_samples_sink = Callback->fft_samples_sink;
  This->on_signal_detected = Callback->on_signal_detected;
  This->on_signal_not_detected = Callback->on_signal_not_detected;
  This->on_dPh_ready = Callback->on_dPh_ready;
  This->on_dA_ready = Callback->on_dA_ready;
  This->on_mis_ready = Callback->on_mis_ready;
  This->on_tis_ready = Callback->on_tis_ready;
  This->on_ds_ready = Callback->on_ds_ready;
  This->on_qam_symbol = Callback->on_qam_symbol;
  This->on_collector_ready = Callback->on_collector_ready;
  This->on_collector_processed = Callback->on_collector_processed;
}

INLINE void NAVDAT_Demodulator_reset(NAVDAT_Demodulator_t* This)
{
  This->isData = 0;
  This->isTIS = 0;
  This->isDS = 0;
  This->FrameCounter = 0;
  This->Xcurrent = 0;
  NAVDAT_DataCounter_reset(&This->DataCounter);
}

void NAVDAT_Demodulator_set_tisMode(NAVDAT_Demodulator_t *This, NAVDAT_TIS_qamMode_t mode);
void NAVDAT_Demodulator_set_dsMode(NAVDAT_Demodulator_t *This, NAVDAT_DS_qamMode_t mode);

INLINE void NAVDAT_Demodulator_UseExternalCollector(NAVDAT_Demodulator_t* This){This->IsExternalCollector = 1;}
void init_NAVDAT_Demodulator(NAVDAT_Demodulator_t* This, NAVDAT_DemodulatorCfg_t* Cfg);

Overflow_t navdat_demodulator_iq(NAVDAT_Demodulator_t* This, iqSample_t* Samples, Size_t Count);
Overflow_t navdat_demodulator_collector_handler(NAVDAT_Demodulator_t* This, iqSample_t* Collector);
Sample_t navdat_demodulator_snr_estimation(NAVDAT_Demodulator_t *This, Size_t start_idx, Size_t stop_idx);

#ifdef __cplusplus
}
#endif

#endif // __NAVDAT_DEMIDULATOR_H_
