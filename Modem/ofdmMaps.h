/*!
 * \file ofdmMaps.h
 * \brief Объявления карт индексов ячеек потоков NAVDAT
 *
 * файл сгенерирован в MatLab
 */

#include "SDR/BASE/common.h"

extern Int8_t* NAVDAT_Pilots_idx[];
extern Size_t NAVDAT_Pilots_idxCount[];

extern Int8_t* NAVDAT_MIS_idx[];
extern Size_t NAVDAT_MIS_idxCount[];

extern Int8_t* NAVDAT_TIS_idx[];
extern Size_t NAVDAT_TIS_idxCount[];

extern Int8_t* NAVDAT_DS_idx[];
extern Size_t NAVDAT_DS_idxCount[];

extern iqSample_t* NAVDAT_Pilots_ph[];
extern Size_t NAVDAT_Pilots_phCount[];

