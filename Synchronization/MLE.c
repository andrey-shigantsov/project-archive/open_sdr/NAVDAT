/*!
 * \file MLE.c
 * \author Шиганцов А.А.
 * \brief Реализация оценки максимального правдоподобия (MLE) временных и частотных сдвигов в OFDM
 */

#include "MLE.h"
#include "sdr_complex.h"
#include "sdr_math.h"
#include "sdr_sine.h"

#include <math.h>

Sample_t NAVDAT_mleSync_epsilant(NAVDAT_mleSync_t* This, iqSample_t* Samples)
{
  Calc_t res = 0;
  Size_t k;
  for (k = 0; k < This->L; k++)
  {
    Sample_t x = iq_magnitude_1(&Samples[k]);
    Sample_t y = iq_magnitude_1(&Samples[k + This->N]);
    res += x*x + y*y;
  }
  res /= 2;
  return res;
}

iqSample_t NAVDAT_mleSync_gamma(NAVDAT_mleSync_t* This, iqSample_t* Samples)
{
  iqSample_t Gamma; Gamma.i = Gamma.q = 0;
  for (Size_t i = 0; i < This->L; ++i)
  {
    iqSample_t ConjugateN;
    ConjugateN.i = Samples[i+This->N].i;
    ConjugateN.q = -Samples[i+This->N].q;

    iqSample_t X = iq_mul(Samples[i], ConjugateN);

    Gamma.i += X.i;
    Gamma.q += X.q;
  }
  return Gamma;
}

NAVDAT_mleSyncEstimation_t NAVDAT_mleSync_estimation(NAVDAT_mleSync_t* This, iqSample_t* Samples)
{
  NAVDAT_mleSyncEstimation_t res;

  iqSample_t Gamma = NAVDAT_mleSync_gamma(This, Samples);
  Sample_t epsilant = NAVDAT_mleSync_epsilant(This, Samples);

  Calc_t argGamma = iq_angle_calc_1(&Gamma);
  res.freq = -(1.0/(SDR_2PI))*argGamma;
  res.lambda = iq_magnitude_1(&Gamma)*cosinus_1(SDR_2PI*res.freq + argGamma) - This->Rho*epsilant;

  return res;
}

static SizeEx_t get_maxIdx(NAVDAT_mleSync_t* This, Size_t mStart, Size_t mStop, iqSample_t* WindowBuf)
{
  SizeEx_t maxIdx = mStart, Count = mStop - mStart;
  SDR_ASSERT(maxIdx >= 0);
  SDR_ASSERT(maxIdx + Count <= This->syncWindow.Window.Size);
  SDR_ASSERT(maxIdx + Count <= This->CorrelationBuf.Size);

  Size_t m;
  for (m = mStart; m < mStop; ++m)
  {
    NAVDAT_mleSyncEstimation_t est = NAVDAT_mleSync_estimation(This, &WindowBuf[m]);
    This->CorrelationBuf.P[m] = est.lambda;
    This->FreqEstimationBuf.P[m] = est.freq;
    if (This->CorrelationBuf.P[m] > This->CorrelationBuf.P[maxIdx])
      maxIdx = m;
  }
  return maxIdx;
}

static Bool_t maxIdx_handler(NAVDAT_mleSync_t* This, SizeEx_t maxIdx, Bool_t isExtMod)
{
  if (!isExtMod)
  {
    Size_t offset = This->L, idxTreshold = This->L/8;

    Bool_t isNext = 0;
    Size_t start = 0, stop = 0;
    if (maxIdx <= offset + idxTreshold)
    {
      isNext = 1;
      start = 0;
      stop = maxIdx+1;
    }
    else if (maxIdx >= (offset + This->N - idxTreshold))
    {
      isNext = 1;
      start = maxIdx;
      stop = offset + This->N + This->L;
    }
    if (isNext)
      return maxIdx_handler(This, get_maxIdx(This, start, stop, This->syncWindow.Window.P), 1);
  }
  This->currentSymbolOffset = maxIdx;
  This->currentFreqOffset = This->FreqEstimationBuf.P[maxIdx];
  return 1;
}

static void window_handler(NAVDAT_mleSync_t* This, iqSample_t* Samples, Size_t Count)
{
  SDR_ASSERT(Count == This->syncWindow.Window.Size);

  if (This->detect_signal)
    if (!This->isSync && !This->detect_signal(This->Parent, Samples, Count))
      return;

  clear_buffer(This->CorrelationBuf.P, This->CorrelationBuf.Size);
  clear_buffer(This->FreqEstimationBuf.P, This->FreqEstimationBuf.Size);

  if (!maxIdx_handler(This, get_maxIdx(This, This->mStart, This->mStop, Samples), 0))
    return;

  if (This->on_symbol_ready)
    This->on_symbol_ready(This->Parent, &Samples[This->currentSymbolOffset + This->L], This->N);

  if (This->isSync)
    iqSyncWindow_refreshSymbolOffset(&This->syncWindow, This->currentSymbolOffset);
}

void init_NAVDAT_mleSync(NAVDAT_mleSync_t *This, NAVDAT_mleSyncCfg_t *Cfg)
{
  This->Parent = 0;
  This->detect_signal = 0;
  This->on_symbol_ready = 0;

  This->N = Cfg->N;
  This->L = Cfg->L;

  This->SymSize = This->N + This->L;
  This->Count = This->N;

  NAVDAT_mleSync_setSNR(This, Cfg->SNR);

  SDR_ASSERT(Cfg->Buf.Size == NAVDAT_mleSYNC_BUFSIZE(Cfg->N,Cfg->L));
  iqSyncWindowCfg_t cfgSync;
  cfgSync.SymbolSize = This->SymSize;
  cfgSync.SymbolPos = This->L + This->N/2;
  cfgSync.WindowBuf = Cfg->Buf;
  init_iqSyncWindow(&This->syncWindow, &cfgSync);
  iqSyncWindowCallback_t callbackSync;
  callbackSync.on_window_ready = (samples_sink_iq_t)window_handler;
  iqSyncWindow_setCallback(&This->syncWindow, This, &callbackSync);

  This->InSync_mStart = cfgSync.SymbolPos - Cfg->MaxOffsetInSync;
  This->InSync_mStop = cfgSync.SymbolPos + Cfg->MaxOffsetInSync;

  SDR_ASSERT(Cfg->CorrelationBuf.Size == NAVDAT_mleSYNC_ESTIMATION_BUFSIZE(Cfg->N,Cfg->L));
  This->CorrelationBuf = Cfg->CorrelationBuf;
  SDR_ASSERT(Cfg->FreqEstimationBuf.Size == NAVDAT_mleSYNC_ESTIMATION_BUFSIZE(Cfg->N,Cfg->L));
  This->FreqEstimationBuf = Cfg->FreqEstimationBuf;

  This->currentSymbolOffset = cfgSync.SymbolPos;
  NAVDAT_mleSync_disable(This);
}
