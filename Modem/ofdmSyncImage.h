/*!
 * \file ofdmSyncImage.h
 * \brief Объявление образа для синхронизации NAVDAT
 *
 * файл сгенерирован в MatLab
 */

#include "SDR/BASE/common.h"

#define NAVDAT_OFDM_SYNCIMAGE_SIZE 128

extern Sample_t NAVDAT_ofdmSyncImage[NAVDAT_OFDM_SYNCIMAGE_SIZE];

extern Sample_t NAVDAT_ofdmDetectSyncImage[NAVDAT_OFDM_SYNCIMAGE_SIZE];

