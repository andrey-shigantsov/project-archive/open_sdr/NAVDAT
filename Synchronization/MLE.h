/*!
 * \file MLE.h
 * \author Шиганцов А.А.
 * \brief Заголовочный файл реализации оценки максимального правдоподобия (MLE) временных и частотных сдвигов в OFDM
 *
 * см. статью {Jan-Jaap van de Beek} ML Estimation of Timing and Frequency Ofset in Multicarrier Systems
 */

#ifndef __NAVDAT_SYNC_MLE_H__
#define __NAVDAT_SYNC_MLE_H__

#include "SDR/BASE/common.h"
#include "SDR/BASE/Abstract.h"
#include "SDR/BASE/Solvers/syncWindow.h"

#ifdef __cplusplus
extern "C"{
#endif

#define NAVDAT_mleSYNC_BUFSIZE(N,L) (2*(N + L) + L)
#define NAVDAT_mleSYNC_ESTIMATION_BUFSIZE(N,L) (N + 2*L)

typedef struct
{
  Size_t N; /* Размер полезной нагрузки символа OFDM */
  Size_t L; /* Размер защитного интервала символа OFDM */
  ValueFloat_t SNR; /* Соотношение сигнал/шум */
  Value_t MaxOffsetInSync; /* Максимальный сдвиг во время синхронизации */
  iqBuffer_t Buf; /* Размер -- NAVDAT_mleSYNC_BUFSIZE */
  Buffer_t CorrelationBuf, FreqEstimationBuf; /* Размер -- NAVDAT_mleSYNC_ESTIMATION_BUFSIZE */
} NAVDAT_mleSyncCfg_t;

typedef struct
{
  samples_solver_iq_t detect_signal;
  samples_sink_iq_t on_symbol_ready;
} NAVDAT_mleSyncCallback_t;

typedef struct
{
  Sample_t lambda;
  Sample_t freq;
} NAVDAT_mleSyncEstimation_t;

typedef struct
{
  void* Parent;
  samples_solver_iq_t detect_signal;
  samples_sink_iq_t on_symbol_ready;

  Bool_t isSync;

  Size_t N, L, SymSize, Count;
  ValueFloat_t Rho;
  Size_t mStart, mStop, InSync_mStart, InSync_mStop;

  iqSyncWindow_t syncWindow;

  Buffer_t CorrelationBuf, FreqEstimationBuf;
  Size_t currentSymbolOffset;
  Sample_t currentFreqOffset;
} NAVDAT_mleSync_t;

INLINE void NAVDAT_mleSync_setCallback(NAVDAT_mleSync_t* This, void* Parent, NAVDAT_mleSyncCallback_t* Callback)
{
  This->Parent = Parent;
  This->detect_signal = Callback->detect_signal;
  This->on_symbol_ready = Callback->on_symbol_ready;
}

INLINE void NAVDAT_mleSync_setSNR(NAVDAT_mleSync_t* This, ValueFloat_t SNR)
{
  if (SNR < 0) return;
  This->Rho = SNR/(SNR+1);
}

INLINE Bool_t NAVDAT_mleSync_isSync(NAVDAT_mleSync_t* This)
{
  return This->isSync;
}

INLINE void NAVDAT_mleSync_enable(NAVDAT_mleSync_t* This)
{
  This->isSync = 1;
  This->mStart = This->InSync_mStart;
  This->mStop = This->InSync_mStop;
}
INLINE void NAVDAT_mleSync_disable(NAVDAT_mleSync_t* This)
{
  This->isSync = 0;
  This->mStart = This->L;
  This->mStop = This->mStart + This->Count;
}

INLINE void NAVDAT_mleSync_reset(NAVDAT_mleSync_t* This)
{
  iqSyncWindow_reset(&This->syncWindow);

  NAVDAT_mleSync_disable(This);
}

NAVDAT_mleSyncEstimation_t NAVDAT_mleSync_estimation(NAVDAT_mleSync_t* This, iqSample_t* Samples);
iqSample_t NAVDAT_mleSync_gamma(NAVDAT_mleSync_t* This, iqSample_t* Samples);
Sample_t NAVDAT_mleSync_epsilant(NAVDAT_mleSync_t* This, iqSample_t* Samples);

void init_NAVDAT_mleSync(NAVDAT_mleSync_t* This, NAVDAT_mleSyncCfg_t* Cfg);

INLINE void navdat_mle_sync_1(NAVDAT_mleSync_t* This, iqSample_t Sample)
{
  sync_window_collector_iq_1(&This->syncWindow, Sample);
}
INLINE void navdat_mle_sync(NAVDAT_mleSync_t* This, iqSample_t* Samples, Size_t Count)
{
  sync_window_collector_iq(&This->syncWindow, Samples, Count);
}


#ifdef __cplusplus
}
#endif

#endif // __NAVDAT_SYNC_MLE_H__
